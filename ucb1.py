__author__ = 'Nirandika'
import math
import pandas as pd
import utilities as utils

class UCB1():
    def __init__(self, counts, values, alpha):
        self.counts = counts
        self.values = values
        self.alpha =alpha
        return

    def initialize(self, n_arms):
        self.counts = [0 for col in range(n_arms)]
        self.values = [0.0 for col in range(n_arms)]
        return

    def ind_max(self, x):
        m = max(x)
        return x.index(m)

    def select_arm(self):
        n_arms = len(self.counts)
        for arm in range(n_arms):
            if self.counts[arm] == 0:
                return arm
        ucb_values = [0.0 for arm in range(n_arms)]
        total_counts = sum(self.counts)
        for arm in range(n_arms):
            bonus = self.alpha / float(self.counts[arm]) # math.sqrt((2 * math.log(total_counts)) / float(self.counts[arm]))
            ucb_values[arm] = self.values[arm] + bonus
        return self.ind_max(ucb_values)

    def update(self, chosen_arm, reward):
        self.counts[chosen_arm] = self.counts[chosen_arm] + 1
        n = self.counts[chosen_arm]
        value = self.values[chosen_arm]
        new_value = ((n - 1) / float(n)) * value + (1 / float(n)) * reward
        self.values[chosen_arm] = new_value
        return


if __name__ == '__main__':
    ucb1 = UCB1([0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 4)
    ucb1.initialize(10)


    input_file = "D:\intellij_projects\Ocean-Bandit\\newFeature.txt"
    result_output_file = "ucb1_result.txt"
    data = pd.read_csv(input_file, header=None)
    # print data.head()
    print data.shape
    total_number_of_trials = 1
    total_number_of_correct_recommendation = 0
    validation_set_true_target = data.ix[:, 20:]
    print validation_set_true_target.head

    for i in range(0, 2000):
            selected_arm = ucb1.select_arm()
            print i
            true_arm=validation_set_true_target.iloc[i][20]
            print "Selected ", selected_arm, "Real ",true_arm
            true_reward=0
            if selected_arm==true_arm:
                print "MATCH"
                total_number_of_correct_recommendation += 1
                true_reward=1


            utils.write_data(total_number_of_trials, total_number_of_correct_recommendation,result_output_file)
            total_number_of_trials +=1
            ucb1.update(selected_arm,true_arm)



