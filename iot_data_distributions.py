__author__ = 'workshop'

from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
import pandas as pd
import matplotlib.pyplot as plt

# input_file = "D:\intellij_projects\Ocean-manual-data-manipulation\context_information.csv"
input_file ="iot_experiment_data_analysis.csv"

data = pd.read_csv(input_file, header=None)
# print data.head()
print data.shape
context_data = data.ix[0:,:10]
user_data = data.ix[0:, 10:21]
arm_used = data.ix[0:, 20:]

print context_data.shape
print user_data.shape
print arm_used

context_pca = PCA(n_components=2).fit(context_data)
context_pca_1d = context_pca.transform(context_data)

user_pca = PCA(n_components=2).fit(user_data)
user_pca_1d = user_pca.transform(user_data)
plt.figure(figsize=(5,3),dpi=300)
# ax = plt.subplot(121)
plt.scatter(context_pca_1d[:,0], context_pca_1d[:,1], c=arm_used)
plt.grid()
# plt.title('IoT service vs context distribution')
plt.xlabel("pca-1")
plt.ylabel("pca-2")
#ax.set_title('IoT service vs context distribution')
'''
ax = plt.subplot(122)
ax.scatter(user_pca_1d[:,0], user_pca_1d[:,1], c=arm_used)
ax.grid()
ax.set_title('IoT service vs users distribution')
'''

plt.show()






