__author__ = 'Nirandika'

from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
import pandas as pd
import pylab as pl

# input_file = "D:\intellij_projects\Ocean-manual-data-manipulation\context_information.csv"
input_file = "D:\intellij_projects\Ocean-manual-data-manipulation\\artificialdata_iot2016_user_features.csv"
user_clustering_output = "context_clusters"

data = pd.read_csv(input_file, header=None)
# print data.head()
print data.shape

training_data = data.ix[1:, :9]
print training_data.shape

pca = PCA(n_components=2).fit(training_data)
pca_2d = pca.transform(training_data)
# for i in range(0,pca_2d.shape[0]):
# if k_means.labels_[i] == 1
#        c1


k_means = KMeans(n_clusters=10, random_state=2)
k_means.fit(training_data)
pl.figure('K-means with 10 clusters')
pl.scatter(pca_2d[:, 0], pca_2d[:, 1], c=k_means.labels_)
pl.show()

for i in range(0, 2000):
    print k_means.labels_[i]


