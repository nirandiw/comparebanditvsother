from __future__ import division

__author__ = 'Nirandika'


def write_data(tot_trials, correction_rec, result_output_file):
    ctr = correction_rec / tot_trials
    print "CTR:", ctr
    with open(result_output_file, 'a') as output:
        output.write("Learning Precision is ")
        output.write(str(correction_rec) + "/" + str(tot_trials) + "=" + str(ctr))
        output.write("\n")
