__author__ = 'Nirandika'
import time
import logging
import pymc3 as pm
import theano
import numpy as np
import scipy as sp
import pandas as pd
import os

try:
    import ujson as json
except ImportError:
    import json


# Enable on-the-fly graph computations, but ignore
# absence of intermediate test values.
theano.config.compute_test_value = 'ignore'

# Set up logging.
logger = logging.getLogger()
logger.setLevel(logging.INFO)


class PMF(object):
    """Probabilistic Matrix Factorization model using pymc3."""

    def __init__(self, train, dim, alpha=2, std=0.01, bounds=(-10, 10)):
        """Build the Probabilistic Matrix Factorization model using pymc3.

        :param np.ndarray train: The training data to use for learning the model.
        :param int dim: Dimensionality of the model; number of latent factors.
        :param int alpha: Fixed precision for the likelihood function.
        :param float std: Amount of noise to use for model initialization.
        :param (tuple of int) bounds: (lower, upper) bound of ratings.
            These bounds will simply be used to cap the estimates produced for R.

        """
        self.dim = dim
        self.alpha = alpha
        self.std = np.sqrt(1.0 / alpha)
        self.bounds = bounds
        self.data = train.copy()
        n, m = self.data.shape

        # Perform mean value imputation
        nan_mask = np.isnan(self.data)
        self.data[nan_mask] = self.data[~nan_mask].mean()

        # Low precision reflects uncertainty; prevents overfitting.
        # Set to the mean variance across users and items.
        self.alpha_u = 1 / self.data.var(axis=1).mean()
        self.alpha_v = 1 / self.data.var(axis=0).mean()

        # Specify the model.
        logging.info('building the PMF model')
        with pm.Model() as pmf:
            U = pm.MvNormal(
                'U', mu=0, tau=self.alpha_u * np.eye(dim),
                shape=(n, dim), testval=np.random.randn(n, dim) * std)
            V = pm.MvNormal(
                'V', mu=0, tau=self.alpha_v * np.eye(dim),
                shape=(m, dim), testval=np.random.randn(m, dim) * std)
            R = pm.Normal(
                'R', mu=theano.tensor.dot(U, V.T), tau=self.alpha * np.ones((n, m)),
                observed=self.data)

        logging.info('done building the PMF model')
        self.model = pmf

    def __str__(self):
        return self.name


    # Now define the MAP estimation infrastructure.
    def _map_dir(self):
        basename = 'pmf-map-d%d' % self.dim
        return os.path.join('data', basename)

    def _find_map(self):
        """Find mode of posterior using Powell optimization."""
        tstart = time.time()
        with self.model:
            logging.info('finding PMF MAP using Powell optimization...')
            self._map = pm.find_MAP(fmin=sp.optimize.fmin_powell, disp=True)

        elapsed = int(time.time() - tstart)
        logging.info('found PMF MAP in %d seconds' % elapsed)

        # This is going to take a good deal of time to find, so let's save it.
        self.save_np_vars(self._map, self.map_dir)

    def _load_map(self):
        self._map = self.load_np_vars(self.map_dir)


    def _map(self):
        try:
            return self._map
        except:
            if os.path.isdir(self.map_dir):
                self.load_map()
            else:
                self.find_map()
            return self._map


    def load_np_vars(self, savedir):
        """Load numpy variables saved with `save_np_vars`."""
        shape_file = os.path.join(savedir, 'shapes.json')
        with open(shape_file, 'r') as sfh:
            shapes = json.load(sfh)

        vars = {}
        for varname, shape in shapes.items():
            var_file = os.path.join(savedir, varname + '.txt')
            vars[varname] = np.loadtxt(var_file).reshape(shape)

        return vars
        # First define functions to save our MAP estimate after it is found.
        # We adapt these from `pymc3`'s `backends` module, where the original
        # code is used to save the traces from MCMC samples.

    def save_np_vars(self, vars, savedir):
        """Save a dictionary of numpy variables to `savedir`. We assume
        the directory does not exist; an OSError will be raised if it does.
        """
        logging.info('writing numpy vars to directory: %s' % savedir)
        os.mkdir(savedir)
        shapes = {}
        for varname in vars:
            data = vars[varname]
            var_file = os.path.join(savedir, varname + '.txt')
            np.savetxt(var_file, data.reshape(-1, data.size))
            shapes[varname] = data.shape

            ## Store shape information for reloading.
            shape_file = os.path.join(savedir, 'shapes.json')
            with open(shape_file, 'w') as sfh:
                json.dump(shapes, sfh)


if __name__ == '__main__':
    data = pd.read_csv("D:\\intellij_projects\\Ocean-manual-data-manipulation\CF_PMF_data.csv", header=None)
    print data.head()
    training_data = data.ix[1:, 1:]
    print training_data.head()
    print training_data.values
    pmf = PMF(training_data.values, 10)
    pmf._find_map()
    pmf._load_map()
    pmf._map_dir()
    pmf._map