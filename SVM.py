from __future__ import division

__author__ = 'Nirandika'

from sklearn.svm import SVC
import pandas as pd
import numpy as np
import seaborn as sms
import utilities as utils


input_file = "D:\intellij_projects\Ocean-manual-data-manipulation\\artificialdata_iot2016_svm_data.csv"
result_output_file = "svm_result.txt"
data = pd.read_csv(input_file, header=None)
# print data.head()
print data.shape

# plt.show(sms.pairplot(data, x_vars=['x1', 'x2', 'x3', 'z1', 'z2', 'z3'], y_vars='Arm', size=7, aspect=0.7))

training_data = data.ix[:99, :19]
target_value = data.ix[:99, 20:]

array_training_data = training_data.values
array_target_value = np.ravel(target_value.values)
print "Training Data"
# print training_data.head()
# print target_value.head()
print training_data.shape, target_value.shape

# print  array_training_data

# print array_target_value # target_value.head()

clf = SVC()
clf.fit(array_training_data, array_target_value)

validation_set = data.ix[:, :19]
validation_set_true_target = data.ix[:, 20:]

print "Validation Data"
print validation_set.head()
print validation_set_true_target.head()
print validation_set.shape, validation_set_true_target.shape

# print validation_set_true_target.iloc[999][20]
total_number_of_trials = 1
total_number_of_correct_recommendation = 0
counter = 0;

for validation_point in validation_set.itertuples():
    print validation_point
    array_validation_point = np.ravel(validation_point[1:]).reshape(-1, 20)
    # print array_validation_point
    predicted_class = clf.predict(array_validation_point)[0]
    print counter
    print "Predicted Class", predicted_class
    print "Real Class ", validation_set_true_target.iloc[counter][20]
    if predicted_class == validation_set_true_target.iloc[counter][20]:
        print "MATCH"
        total_number_of_correct_recommendation += 1

    utils.write_data(total_number_of_trials, total_number_of_correct_recommendation, result_output_file)
    total_number_of_trials += 1
    counter += 1





