__author__ = 'Nirandika'

import pandas as pd
import utilities as utils
from scipy.spatial.distance import cosine

data = pd.read_csv("D:\intellij_projects\Ocean-manual-data-manipulation\\CF_simple_data.csv")
print data.head()
# training_data= data.ix[1:,1:]
#print training_data.head()

data_item = data.drop('user', 1)
data_ibs = pd.DataFrame(index=data_item.columns, columns=data_item.columns)


# Lets fill in those empty spaces with cosine similarities
# Loop through the columns
for i in range(0, len(data_ibs.columns)):
    # Loop through the columns for each column
    for j in range(0, len(data_ibs.columns)):
        # Fill in placeholder with cosine similarities
        data_ibs.ix[i, j] = 1 - cosine(data_item.ix[:, i], data_item.ix[:, j])

# Create a placeholder items for closes neighbours to an item
data_neighbours = pd.DataFrame(index=data_ibs.columns, columns=range(1, 11))

# Loop through our similarity dataframe and fill in neighbouring item names
for i in range(0, len(data_ibs.columns)):
    data_neighbours.ix[i, :10] = data_ibs.ix[0:, i].sort_values(ascending=False)[:10].index

# --- End Item Based Recommendations --- #

print "Item based recommendation \n", data_neighbours.head(10).ix[:10, 1:11]


# --- End Item Based Recommendations --- #

# --- Start User Based Recommendations --- #

# Helper function to get similarity scores
def getScore(history, similarities):
    return sum(history * similarities) / sum(similarities)

# Create a place holder matrix for similarities, and fill in the user name column
data_sims = pd.DataFrame(index=data.index, columns=data.columns)
data_sims.ix[:, :1] = data.ix[:, :1]

print "data_sims ", data_sims.head()

#Loop through all rows, skip the user column, and fill with similarity scores
for i in range(0, len(data_sims.index)):
    for j in range(1, len(data_sims.columns)):
        user = data_sims.index[i]
        product = data_sims.columns[j]

        if data.ix[i][j] == 1:
            data_sims.ix[i][j] = 0
        else:
            product_top_names = data_neighbours.ix[product][1:10]
            product_top_sims = data_ibs.ix[product].sort_values(ascending=False)[1:10]
            user_purchases = data_item.ix[user, product_top_names]

            data_sims.ix[i][j] = getScore(user_purchases, product_top_sims)

# Get the top songs
data_recommend = pd.DataFrame(index=data_sims.index,
                              columns=['user', 'a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a9', 'a10'])
data_recommend.ix[0:, 0] = data_sims.ix[:, 0]

# Instead of top song scores, we want to see names
for i in range(0, len(data_sims.index)):
    data_recommend.ix[i, 1:] = data_sims.ix[i, :].sort_values(ascending=False).ix[1:11, ].index.transpose()

# Print a sample
print data_recommend.ix[:10, :11]

testing_data = pd.read_csv("D:\intellij_projects\Ocean-manual-data-manipulation\\artificialdata_iot2016_CF_simple_testing_data.csv",
                           header=None)

print "Testing data shape", testing_data.shape
selected_arms = testing_data.ix[:, :0]
testing_users = testing_data.ix[:, 1:]

print "Selected arms", selected_arms

print "Testing users", testing_users.shape

total_number_of_trials = 1
total_number_of_correct_recommendation = 0
for i in range(0, 2000):
    user = testing_users.iloc[i][1]
    # print user
    recommended_arm_1 = data_recommend.iloc[user]['a1'].replace('a', "")
    recommended_arm_2 = data_recommend.iloc[user]['a2'].replace('a', "")
    recommended_arm_3 = data_recommend.iloc[user]['a3'].replace('a', "")
    recommended_arm_4 = data_recommend.iloc[user]['a4'].replace('a', "")
    selected_arm = selected_arms.iloc[i][0]
    # recommended_arm = recommended_arm_1.replace('a', "")

    print "Recommended arm ", recommended_arm_1," ", recommended_arm_2," ", recommended_arm_3," ", recommended_arm_3, "Real Arm ", selected_arms.iloc[i][0]
    if recommended_arm_1 == str(selected_arm) or recommended_arm_2 == str(selected_arm) or recommended_arm_3 == str(selected_arm) or recommended_arm_4 == str(selected_arm):
        "MATCH"
        total_number_of_correct_recommendation += 1
    total_number_of_trials += 1
    utils.write_data(total_number_of_trials, total_number_of_correct_recommendation, "CF_results.txt")








